# UniFi Controller

This is a docker container for running the [UniFi AC](https://www.ubnt.com/download/unifi/) controller.

## Usage:

## Versions:

Please select a version from the VERSIONS.md file and insert it appropriately in the following commands..

## Standard Usage:

`docker run -d -p 8080:8080 -p 8443:8443 -p 8880:8880 -p 8843:8843 -p 3478:3478 --name unifi ashneo76/unifi:5.0.7`

### Exporting data:

`docker run -d -v /opt/unifi/data:/unifi/UniFi/data -p 8080:8080 -p 8443:8443 -p 8880:8880 -p 8843:8843 -p 3478:3478  --name unifi ashneo76/unifi:5.0.7`

## Automated Restart:

1. Use systemd file: [unifi.service](https://gitlab.com/ashneo76/unifi/blob/master/unifi.service)
    0. `cp unifi.service /usr/lib/systemd/system`
    1. `systemctl enable unifi`
    2. `systemctl start unifi`

2. Use docker flag `--restart=always`
