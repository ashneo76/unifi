.PHONY: build

DOCKER = $(shell which docker)
IMAGE_NAME = "ashneo76/unifi"
IMAGE_VERSION = "5.0.7"

build:
	$(DOCKER) build -t $(IMAGE_NAME):$(IMAGE_VERSION) .

run:
	mkdir -p data
	$(DOCKER) run -d -v $(shell pwd)/data:/unifi/UniFi/data/ -p 8080:8080 -p 8443:8443 -p 8880:8880 -p 8843:8843 -p 3478:3478 -p 8081:8081 --name unifi $(IMAGE_NAME):$(IMAGE_VERSION)

deploy:
	$(DOCKER) login
	$(DOCKER) push $(IMAGE_NAME):$(IMAGE_VERSION)
